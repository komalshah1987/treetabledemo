import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FileSystemRootNode } from './file-system-root-node';
import { FileSystemNode } from './file-system-node';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  constructor(private http: HttpClient) { }

  getFilesystem() {
    return this.http.get('assets/data/filesystem.json')
      .toPromise()
      .then(res => {
        let files: FileSystemRootNode[] = [];

        for (let i = 0; i < (<any>res).data.length; i++) {
          let rootObj = (<any>res).data[i];
          files.push(new FileSystemRootNode(rootObj));
        }
        return files;
      });
  }
}
