import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator } from '@angular/material';

export interface Student {
  id: Number;
  name: String;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  student: Student = { id: 0, name: "" };
  displayedColumns: string[] = ['id', 'name'];
  dataSource: MatTableDataSource<Student> = new MatTableDataSource<Student>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.data = [
      { id: 1, name: "Karthik" },
      { id: 2, name: "Kalaimani" },
      { id: 3, name: "Jeevesh" },
      { id: 4, name: "Sivanya" }
    ];
  }

  onAdd() {
    let students: Student[] = this.dataSource.data;
    this.student.id = students.length + 1;
    students.push(this.student);
    this.dataSource.data = students;
    this.student = { id: 0, name: "" };
  }
}
