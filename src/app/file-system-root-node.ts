import { TreeNode } from 'primeng/api/treenode';
import { FileSystemNode } from './file-system-node';
import { Observable } from 'rxjs';

export class FileSystemRootNode implements TreeNode {
    data: FileSystemNode;
    children: FileSystemNode[];

    total: number;

    constructor(data: any) {
        this.data = Object.assign(new FileSystemNode(data.data, data.children, data), data.data);
        this.children = [];
        if (data.children) {
            for (let i = 0; i < data.children.length; i++) {
                this.children[i] = Object.assign(new FileSystemNode(data.children[i], data.children[i].children, this.data), data.children[i]);
            }
        }
    }
}