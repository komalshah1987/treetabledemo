import { Component, OnInit } from '@angular/core';
import { NodeService } from '../node.service';
import { FileSystemRootNode } from '../file-system-root-node';

@Component({
  selector: 'app-tree-table-demo',
  templateUrl: './tree-table-demo.component.html',
  styleUrls: ['./tree-table-demo.component.scss']
})
export class TreeTableDemoComponent implements OnInit {

  files: FileSystemRootNode[];
  cols: any[];
  editableCols: any[];

  constructor(private nodeService: NodeService) { }

  ngOnInit() {
    this.nodeService.getFilesystem().then(files => {
      this.files = files
    });

    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'size', header: 'Size' },
      { field: 'ratio', header: 'Ratio' },
      { field: 'type', header: 'Type' }
    ];

    this.editableCols = [
      { field: 'size', header: 'Size' }
    ];
  }

  change() {
    // this.files[0].data.name = "XYZ";
  }

}
