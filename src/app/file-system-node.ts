import { TreeNode } from 'primeng/api/treenode';

export class FileSystemNode implements TreeNode {
    _name: String;
    _size: number = 0;
    _ratio: number = 0;
    _fileType: String;
    data: FileSystemNode;//current Object
    children: FileSystemNode[];//Children of current Object
    parent:FileSystemNode;//Parent of current Object

    constructor(data: FileSystemNode, children: FileSystemNode[], parent:FileSystemNode) {
        this.data = data;
        this.children = [];
        if (children != undefined && children.length > 0) {
            for (let i = 0; i < children.length; i++) {
                this.children[i] = Object.assign(new FileSystemNode(children[i], children[i].children,children[i].parent ), this.cast(children[i]));
            }
        }
    }

    cast(child: any) {
        if (child.data) {
            child.data = Object.assign(new FileSystemNode(child.data, child.children, child.parent), child.data);
            if (child.data.children && child.data.children.length > 0) {
                for (let i = 0; i < child.data.children.length; i++) {
                    this.cast(child.data.children[i]);
                }
            }
        }
        if (child.children && child.children.length > 0) {
            for (let i = 0; i < child.children.length; i++) {
                this.cast(child.children[i]);
            }
        }
        return child;
    }


    get fileType(): String {
        return this._fileType;
    }

    set fileType(val: String) {
        this._fileType = val;
    }

    get ratio(): number {
        return this._ratio;
    }

    set ratio(val: number) {
        this._ratio = val;
        if (this.children && this.children.length > 0) {
            for (let i = 0; i < this.children.length; i++) {
                let child = this.children[i].data;
                child.ratio = val / this.children.length;
                this.changeRatioRecursive(this.children[i]);
            }
        }
    }

    changeRatioRecursive(child: any) {
        let val: number = child.data.ratio;
        if (child && child.children != undefined && child.children.length > 0) {
            for (let i = 0; i < child.children.length; i++) {
                let child1 = child.children[i].data;
                child1.ratio = val / child.children.length;
                this.changeRatioRecursive(child1);
            }
        } else if (child.data) {
            child.data.ratio = val;
        }
    }

    get name(): String {
        return this._name;
    }

    set name(val: String) {
        this._name = val;
    }

    get size(): number {
        return this._size;
    }

    set size(val: number) {
        this._size = val;
        // if (this._size > 0 && this.children.length > 0) {
        //     for (let i = 0; i < this.children.length; i++) {
        //         if (this.children[i]._size > 0) {
        //             this.children[i].ratio = (this._size / this.children[i]._size);
        //         }
        //     }
        // }
    }
}